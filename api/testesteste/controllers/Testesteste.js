'use strict';

/**
 * Testesteste.js controller
 *
 * @description: A set of functions called "actions" for managing `Testesteste`.
 */

module.exports = {

  /**
   * Retrieve testesteste records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.testesteste.search(ctx.query);
    } else {
      return strapi.services.testesteste.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a testesteste record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.testesteste.fetch(ctx.params);
  },

  /**
   * Count testesteste records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.testesteste.count(ctx.query);
  },

  /**
   * Create a/an testesteste record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.testesteste.add(ctx.request.body);
  },

  /**
   * Update a/an testesteste record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.testesteste.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an testesteste record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.testesteste.remove(ctx.params);
  }
};
